import React, { useState } from "react";

const Rectangulo = () => {
  const [redColor, setRedColor] = useState(0);
  const [greenColor, setGreenColor] = useState(0);
  const [blueColor, setBlueColor] = useState(0);
  const [flag, SetFlag] = useState(true);

  function MouseOverCapture (e){
    if (flag) {
      const element = document.getElementById("IDrectangulo");
      const a = element.offsetLeft;
      const b = element.offsetTop;
      const x = e.clientX;
      const y = e.clientY;
      setBlueColor(y - b);
      setRedColor(x - a);
    }
  };

  function stopColor() {
    SetFlag(false);
  }

  function starColor(){
    SetFlag(true);
  }

  return (
    <div>
      <div
        id="IDrectangulo"
        style={{
          width: "255px",
          height: "255px",
          backgroundColor: `rgb(${redColor}, ${greenColor}, ${blueColor})`,
        }}
        onMouseOut={starColor}
        onDoubleClick={stopColor}
        onMouseMoveCapture={MouseOverCapture}
      ></div>
    </div>
  );
};

export default Rectangulo;
