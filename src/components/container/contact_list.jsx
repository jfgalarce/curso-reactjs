import React, { useState, useEffect } from "react";
import { Contact } from "../../models/contact.class";
import { GENDER } from "../../models/gender.enum";
import { STATE } from "../../models/state.enum";
import ContactComponent from "../pure/contact";
import ContactForm from "../pure/form/contactForm";

const ContactList = () => {

  const [contacts, setContacts] = useState([]);

  function addContacto(contact) {
    const tempContact = [...contacts];
    tempContact.push(contact);
    setContacts(tempContact);
  }

  function deleteContact(contact) {
    const index = contacts.indexOf(contact);
    const tempContact = [...contacts];
    tempContact.splice(index, 1);
    setContacts(tempContact);
  }

  function updateStateContact(contact) {
    const index = contacts.indexOf(contact);
    const tempContact = [...contacts];
    if (tempContact[index].state === STATE.ONLINE) {
      tempContact[index].state = STATE.OFFLINE;
    } else {
      tempContact[index].state = STATE.ONLINE;
    }

    setContacts(tempContact);
  }

  return (
    <div className="container text-center border rounded-3 m-3">
      <ContactForm add={addContacto}></ContactForm>
      <div className="row justify-content-md-center m-3">
        <div className="col  border rounded-3">
          <table className="table  ">
            <thead>
              <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phono</th>
                <th scope="col">Gender</th>
                <th scope="col" colSpan={2}>
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>
              {contacts.map((contact, index) => {
                return (
                  <ContactComponent
                    key={contact}
                    contact={contact}
                    stated={updateStateContact}
                    remove={deleteContact}
                  ></ContactComponent>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default ContactList;
