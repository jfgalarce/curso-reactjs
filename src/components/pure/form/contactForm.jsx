import React, { useRef } from "react";
import PropTypes from "prop-types";
import { GENDER } from "../../../models/gender.enum";
import { STATE } from "../../../models/state.enum";
import { Contact } from "../../../models/contact.class";
const ContactForm = ({ add }) => {
  const firstNameRef = useRef("");
  const lastNameRef = useRef("");
  const phonoRef = useRef("");
  const emailRef = useRef("");
  const genderRef = useRef("");
  function addContact(e) {
    e.preventDefault();
    const newContact = new Contact(
      firstNameRef.current.value,
      lastNameRef.current.value,
      phonoRef.current.value,
      emailRef.current.value,
      genderRef.current.value,
      STATE.ONLINE
    );
    add(newContact);
    firstNameRef.current.value='';
    lastNameRef.current.value='';
    phonoRef.current.value='';
    emailRef.current.value='';
    genderRef.current.value = '';
  }


  return (
    <form onSubmit={addContact}>
      <div className="row justify-content-md-center m-3">
        <div className="col-6">
          <div className="input-group input-group-lg m-3">
            <span className="input-group-text" id="basic-addon1">
              <strong>First Name</strong>
            </span>
            <input
              ref={firstNameRef}
              type="text"
              id="inputFirstName"
              className="form-control"
              placeholder="First Name"
              aria-label="First Name"
              aria-describedby="basic-addon1"
              required autoFocus
            />
          </div>
          <div className="input-group input-group-lg m-3">
            <span className="input-group-text" id="basic-addon1">
              <strong>Last Name</strong>
            </span>
            <input
            ref={lastNameRef}
              type="text"
              id="inputLastName"
              className="form-control"
              placeholder="Last Name"
              aria-label="Last Name"
              aria-describedby="basic-addon1"
              required
            />
          </div>
          <div className="input-group input-group-lg m-3">
            <span className="input-group-text" id="basic-addon1">
              <strong>Email</strong>
            </span>
            <input
            ref={emailRef}
              type="text"
              id="inputEmail"
              className="form-control"
              placeholder="Email"
              aria-label="Email"
              aria-describedby="basic-addon1"
              required
            />
          </div>
          <div className="input-group input-group-lg m-3">
            <span className="input-group-text" id="basic-addon1">
              <strong>Phono</strong>
            </span>
            <input
                ref={phonoRef}
              type="text"
              id="inputPhono"
              className="form-control"
              placeholder="Phono"
              aria-label="Phono"
              aria-describedby="basic-addon1"
              required
            />
          </div>
          <div className="input-group input-group-lg m-3">
            <label className="input-group-text" for="inputGroupSelect01">
              <strong>Gender</strong>
            </label>
            <select className="form-select" id="selectGender" ref={genderRef} required>
              {Object.keys(GENDER).map((key) => (
                <option value={GENDER[key]}>{GENDER[key]}</option>
              ))}
            </select>
          </div>
          <button type="submit" className="btn btn-primary btn-lg">
            Save
          </button>
        </div>
      </div>
    </form>
  );
};

ContactForm.propTypes = {
    add: PropTypes.func.isRequired
};

export default ContactForm;
