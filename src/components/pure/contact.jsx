import React from "react";
import PropTypes from "prop-types";
import { Contact } from "../../models/contact.class";
import { STATE } from "../../models/state.enum";
const ContactComponent = ({contact, stated, remove}) => {
    function contactState(){
        if(contact.state===STATE.ONLINE){
            return (
                <i className="bi bi-toggle2-on display-6  text-success" style={{cursor: 'pointer'}} onClick={()=> stated(contact) } ></i>
            );
        }else{
            return (
                <i className="bi bi-toggle2-off display-6  text-secondary"  style={{cursor: 'pointer'}} onClick={()=> stated(contact) } ></i>
            );
        }
    }


  return (
    <tr>
      <td>{ contact.first_name}</td>
      <td>{ contact.last_name}</td>
      <td>{ contact.email}</td>
      <td>{ contact.phono}</td>
      <td>{ contact.gender}</td>
      <td>
        {contactState()}
      </td>
      <td>
        <i className="bi bi-x-circle-fill display-6  text-danger"  style={{cursor: 'pointer'}} onClick={()=>remove(contact)}></i>
      </td>
    </tr>
  );
};

ContactComponent.propTypes = {
  contact: PropTypes.instanceOf(Contact).isRequired,
  stated: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired
};

export default ContactComponent;
