export const GENDER = {
  EMPTY: "",
  MALE: "Masculino",
  FEMALE: "Femenino",
  ABIMEGENER: "Abimegénero",
  AGENERO: "Agénero",
  ANDROGINO: "Andrógino",
  APORAGENERO: "Aporagénero",
  ASTRALGENER: "Astralgénero",
  AUTIGENERO: "Autigénero",
  BIGENERO: "Bigénero",
  CAVUSGENER: "Cavusgénero",
  CENDGENER: "Cendgénero",
  CETEROGENERO: "Ceterogénero",
  COLLGENDER: "Collgender",
  DEMIGENERO: "Demigénero",
  DELICIAGENERO: "Deliciagénero",
  DIGIGENERO: "Digigénero",
  DISRUPTER: "Disrupter",
  DURAGENERO: "Duragénero",
  EGOGENERO: "Egogénero",
  EMERGENERO: "Emergénero",
  FAEGENERO: "Faegénero",
  FASCIGENERO: "Fascigénero",
  FEMENINO_NO_BINARIO: "Femenino no binario",
  FICTIGENERO: "Fictigénero",
  FLUIDO: "Fluido",
  GEMELGENER: "Gemelgénero",
  GENDERVEX: "Gendervex",
  GENDER_NON_CONFORMING: "No conforme con el género",
  GLITCHGENDER: "Glitchgender",
  GRAUGENERO: "Graugénero",
  GRAYGENDER: "Graygender",
  HELIOGENER: "Heliogénero",
  HYDROGENER: "Hydrogénero",
  IMPERIGENERO: "Imperigénero",
  INTERGENERO: "Intergénero",
  JUXERA: "Juxera",
  LIBRAGENERO: "Libragénero",
  LUDOGENERO: "Ludogénero",
  MASCULINO_NO_BINARIO: "Masculino no binario",
  MAGIGENERO: "Magigénero",
  MULTIGENERO: "Multigénero",
  NANOGENERO: "Nanogénero",
  NONEX: "Nonex",
  NOVIGENERO: "Novigénero",
  NULL_GENERO: "Género nulo",
  OMNGENERO: "Omngénero",
  PANGENERO: "Pangénero",
  QUEER: "Queer",
  SIMULOGENERO: "Simulogénero",
  TRIGENERO: "Trigénero",
  VAPOGENER: "Vapogénero",
  VIVID_GENERO: "Género vívido",
  XENOGENERO: "Xenogénero",
};
