import { GENDER } from "./gender.enum";
import { STATE } from "./state.enum";

export class Contact {
  first_name = "";
  last_name = "";
  email = "";
  phono = "";
  gender = GENDER.EMPTY;
  state = STATE.ONLINE;
  constructor(first_name, last_name, email, phono, gender, state) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phono = phono;
    this.gender = gender;
    this.state = state;
  }
}
