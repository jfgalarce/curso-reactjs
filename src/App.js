import './App.css';
import ContactList from './components/container/contact_list';
import Rectangulo from './components/rectangulo';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <ContactList></ContactList> */}
        <Rectangulo></Rectangulo>
      </header>
    </div>
  );
}

export default App;
